import pygame, random

# Constants
WIN_WIDTH   = 400
WIN_HEIGTH  = 520
BLACK       = (0,0,0)
BLUE        = (0,191,255)
GRAY        = (220,220,220)
GREEN       = (34,139,34)
RED         = (240,128,128)
YELLOW      = (255,255,0)
WHITE       = (255,255,255)
# Global variables
number_of_guesses   = 1
color_peg_list      = []
color_list = [BLACK,BLUE,GREEN,RED,YELLOW,WHITE]
AI = None

class DrawCircles():
    def __init__(self,x_pos,y_pos,circle_color = 0, check_box = None, radius = 20):
        if(check_box is not None):
            self.image   = pygame.image.load("checkmark.png")
            self.image   = pygame.transform.scale(self.image,(40,40))
            game_window.blit(self.image,(x_pos,y_pos-20))   # y_pos minus twenty b/c blit takes center line not upper left hand coordinates (Blit required if not using sprites redraw)
        else:
            self.image       = pygame.Surface([40,40])
        self.image.set_colorkey(GRAY)
        self.peg_color_index = circle_color
        if(check_box is None):
            # Draw circle (a predetermined radius) if not looking to create check box
            pygame.draw.circle(game_window,color_list[self.peg_color_index],(x_pos,y_pos),radius)
        self.rect       = self.image.get_rect()
        self.rect.x     = x_pos
        self.rect.y     = y_pos
    def changeColor(self):
        if(self.peg_color_index < 5):
            self.peg_color_index += 1
        else:
            self.peg_color_index = 0
        pygame.draw.circle(game_window,color_list[self.peg_color_index],(self.rect.x,self.rect.y),20)
class ComputerBrainLogic():
    guess_me_list = []
    def __init__(self):
        self.createGuessPegs()
    def createGuessPegs(self):
        for i in range(4):
            self.guess_me_list.append(random.randrange(0,6))
        '''
        # self.guess_me_list = [0,0,3,5]             # <<< debugging by hard coding randomly generated computer pegs!! (2/24/2020)
        print("Guess list: {}".format(self.guess_me_list))'''
    def checkIfUserGuessedCorrectly(self):
        self.tmpUser = color_peg_list[number_of_guesses-1]
        self.tmpUser = [self.tmpUser[x].peg_color_index for x in range(4)]
        self.tmpComp = self.guess_me_list.copy()
        self.match_list = []

        self.i = 0
        while self.i < len(self.tmpUser):
            if(self.tmpComp[self.i] == self.tmpUser[self.i]):
                self.match_list.append("W")
                self.tmpUser.pop(self.i)
                self.tmpComp.pop(self.i)
            else:
                self.i += 1
        self.i = 0
        while self.i < len(self.tmpUser):
            if(self.tmpUser[self.i] in self.tmpComp):
                self.match_list.append("B")
                self.tmpComp.remove(self.tmpUser[self.i])
                self.tmpUser.remove(self.tmpUser[self.i])
            else:
                self.i += 1
        # match_list[] will contain the correct guesses.  W for right location, B for just right color guess

        '''
        print(self.match_list)
        print("tmpUser array: {}".format(self.tmpUser))
        print("tmpComp array: {}".format(self.tmpComp))
        print("------------")
        #This section was used for debugging
        '''

        self.createAnswerPegs()
    def createAnswerPegs(self):
        global number_of_guesses
        if("".join(self.match_list) == "WWWW"):
            # User won!
            column_counter = 345
            # clear checkbox
            pygame.draw.rect(game_window,GRAY,(column_counter,((number_of_guesses * 42) - 20),40,40))
            for i in range(4):
                DrawCircles(column_counter,(number_of_guesses * 42),circle_color=5,radius=5)
                column_counter += 15
            number_of_guesses = 100
        elif(number_of_guesses <= 10):
            column_counter = 345
            row_determiner = number_of_guesses * 42
            # clear checkbox
            pygame.draw.rect(game_window,GRAY,(column_counter,(row_determiner-20),40,40))
            for i in self.match_list:
                if(i == "B"):
                    DrawCircles(column_counter,row_determiner,circle_color=0,radius=5)
                elif(i == "W"):
                    DrawCircles(column_counter,row_determiner,circle_color=5,radius=5)
                column_counter += 15
            # Since user didn't win, increment global number_of_guesses by 1 then call createRowOfCircles function
            number_of_guesses += 1
            if(number_of_guesses <= 10):
                createRowOfCircles()
            if(number_of_guesses == 11):
                # Player loss
                createAnswerCircles()
        else:
            pass    # Do nothing

def createAnswerCircles():
    row_determiner = 490
    column_counter = 1
    pygame.draw.line(game_window,BLACK,(0,460),(WIN_WIDTH-70,460))
    for i in AI.guess_me_list:
        DrawCircles(column_counter * 60, row_determiner,i)
        column_counter += 1
def createRowOfCircles():
    row_determiner = number_of_guesses * 42
    column_counter = 1
    row_list = []
    # Used to create row of colored pegs
    for i in range(4):
        row_list.append(DrawCircles(column_counter * 60, row_determiner))
        column_counter += 1
    # Used to create Submit button
    row_list.append(DrawCircles(345, row_determiner,check_box="yes"))
    color_peg_list.append(row_list)
def pauseGame():
    # infinite loop until quit is selected
    while True:
        for event in pygame.event.get():
            if(event.type == pygame.QUIT):
                return False
def gameLogic():
    # Loop
    continue_game = True
    while continue_game:
        # Event listener for the game
        for event in pygame.event.get():
            if(event.type == pygame.MOUSEBUTTONDOWN):
                # 1 is the left mouse button, 2 is middle, 3 is right.
                if(event.button == 1):
                    x, y = pygame.mouse.get_pos()
                    # This if logic is used to prevent more clicking after 10 guesses -- bug
                    if(number_of_guesses <= 10):
                        for i in range(4):
                            if(color_peg_list[number_of_guesses-1][i].rect.collidepoint(x,y)):
                                color_peg_list[number_of_guesses-1][i].changeColor()
                        if(color_peg_list[number_of_guesses-1][4].rect.collidepoint(x,y)):
                            # Time to compare user's guess to computer generated pegs
                            AI.checkIfUserGuessedCorrectly()
                    else:
                        AI.createAnswerPegs()
            if(event.type == pygame.QUIT):
                continue_game = False
            '''if(number_of_guesses >= 11):
                print("Player loses")
                continue_game = pauseGame()'''
        # Refresh root window
        pygame.display.flip()
        # FPS
        pygame.time.Clock().tick(60)

# Initialize Pygame
pygame.init()
# Screen object attributes
game_window = pygame.display.set_mode((WIN_WIDTH,WIN_HEIGTH))
pygame.display.set_caption("Mastermind")
game_window.fill(GRAY)
pygame.draw.line(game_window,BLACK,(WIN_WIDTH - 70,0),(WIN_WIDTH-70,800))
# Draw circles (1st time)
createRowOfCircles()
# Create guessing pegs
AI = ComputerBrainLogic()
# Start game logic
gameLogic()
# End game
pygame.quit()