'''usersGuest  = [[1,2,3,4,5],[2,3,3,5,1],[4,2,1,2,5],[2,2,2,2,2]]
computer    = [4,2,1,2,5]
match_list = []

tmpUser = usersGuest[1].copy()
tmpComp = computer.copy()

i = 0
while i < len(tmpUser):
    if(tmpComp[i] == tmpUser[i]):
        print("Same location!")
        match_list.append("W")
        tmpUser.pop(i)
        tmpComp.pop(i)
    else:
        i += 1
for i in tmpUser:
    if(i in tmpComp):
        print("In there but wrong location")
        match_list.append("B")

print(match_list)'''

'''take1 = [1,2,3,4,5]
take2 = [9,8,7,6,5]
match = take1.copy()
print(match)
match = take2.copy()
print(match)'''

'''import re

my_list = ['W','W','W','W']
print("".join(my_list))'''


#[i for i in range(input())]
'''x = ["1","2","3","4","5"]
print(*map(int,x))'''

#----------------------------------------------------------------------------------

import re, collections

# Problem Name is &&& ApacheLog &&& PLEASE DO NOT REMOVE THIS LINE.

"""
Instructions to candidate.
 1) Run this code in the REPL to observe its behaviour. The
    execution entry point is specified at the bottom.
 2) Consider adding some additional tests in do_tests_pass().
 3) Implement find_top_ip_address() correctly.
 4) If time permits, try to improve your implementation.
"""


'''def find_top_ip_address(lines):
    """ Given an Apache log file, return IP address(es) which accesses the site most often.

        Our log is in this format (Common Log Format). One entry per line and it starts with an IP address which accessed the site,
        followed by a whitespace.

        10.0.0.1 - frank [10/Dec/2000:12:34:56 -0500] "GET /a.gif HTTP/1.0" 200 234

        Log file entries are passed as a list.
    """

    # todo: implement logic
    my_list = []

    for i in lines:
        test = re.search('^([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)', i)
        my_list.append(test.group(0))

    keep_count = 0
    current_winner = ""
    x = collections.Counter(my_list)
    for key,value in x.items():
        if(value > keep_count):
            current_winner = key
            keep_count = value
    return(current_winner)

def do_tests_pass():
    """Returns True if the test passes. Otherwise returns False."""

    # todo: implement more tests
    lines = ["10.0.0.1 - frank [10/Dec/2000:12:34:56 -0500] \"GET /a.gif HTTP/1.0\" 200 234",
             "10.0.0.1 - frank [10/Dec/2000:12:34:57 -0500] \"GET /b.gif HTTP/1.0\" 200 234",
             "10.0.0.2 - nancy [10/Dec/2000:12:34:58 -0500] \"GET /c.gif HTTP/1.0\" 200 234",
             "10.0.0.2 - nancy [10/Dec/2000:12:34:58 -0500] \"GET /c.gif HTTP/1.0\" 200 234",
             "10.0.0.2 - nancy [10/Dec/2000:12:34:58 -0500] \"GET /c.gif HTTP/1.0\" 200 234",
             "10.0.0.2 - nancy [10/Dec/2000:12:34:58 -0500] \"GET /c.gif HTTP/1.0\" 200 234"]

    result = find_top_ip_address(lines)
    if result == "10.0.0.1":
        print("test passed")
        return True
    else:
        print("test failed")
        return False

if __name__ == "__main__":
    do_tests_pass()'''


#---------------------------------------------------------
row_list = [1,2,3,4]
print(row_list.index(5))
